ARG MPI_FLAVOR=mpich

FROM debian:12 AS base
ARG LLVM_VERSION=14

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Berlin
ENV PATH=/opt/must/bin:$PATH

RUN apt-get update && apt-get -y -qq --no-install-recommends install \
    build-essential \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update \
    && apt-get -y -qq --no-install-recommends install \
    cmake \
    curl \
    binutils-dev \
    make \
    automake \
    autotools-dev \
    autoconf \
    libtool \
    zlib1g \
    zlib1g-dev \
    libatomic1 \
    libfabric-dev \
    libxml2-dev \
    python3 \
    python3-pip \
    python3-venv \
    gfortran \
    gcc \
    g++ \
    git \
    graphviz \
    libgtest-dev \
    clang-${LLVM_VERSION} \
    libomp-${LLVM_VERSION}-dev \
    libclang-${LLVM_VERSION}-dev \
    libclang-rt-${LLVM_VERSION}-dev \
    clang-format-${LLVM_VERSION} \
    clang-tidy-${LLVM_VERSION} \
    llvm-${LLVM_VERSION} \
    lldb-${LLVM_VERSION} \
    ninja-build \
    vim \
    openssh-client \
    gdb \
    wget \
    googletest \
    && apt-get -yq clean \
    && rm --recursive --force /var/lib/apt/lists/*

# Ensure that correct LLVM toolset is used
RUN ln -s /usr/bin/FileCheck-${LLVM_VERSION} /usr/bin/FileCheck
RUN ln -s /usr/bin/clang-${LLVM_VERSION} /usr/bin/clang
RUN ln -s /usr/bin/clang++-${LLVM_VERSION} /usr/bin/clang++
RUN ln -s /usr/bin/clang-format-${LLVM_VERSION} /usr/bin/clang-format

# Install lit
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip3 install --no-input --no-cache-dir --disable-pip-version-check lit==14.0.0
ENV CC=clang
ENV CXX=clang++


FROM base AS base-mpich
RUN apt-get update \
    && apt-get -y -qq --no-install-recommends install \
    mpich \
    libmpich-dev \
    && apt-get -yq clean \
    && rm --recursive --force /var/lib/apt/lists/*

ENV MPICH_CC=clang
ENV MPICH_CXX=clang++


FROM base AS base-openmpi
RUN apt-get update \
    && apt-get -y -qq --no-install-recommends install \
    openmpi-bin \
    libopenmpi-dev \
    && apt-get -yq clean \
    && rm --recursive --force /var/lib/apt/lists/*

ENV OMPI_CC=clang
ENV OMPI_CXX=clang++


FROM base-${MPI_FLAVOR} AS final
ARG MUST_VERSION=1.9.2

COPY patches /patches

# Build and install MUST
RUN mkdir -p /opt/must/
WORKDIR /build
RUN wget https://hpc.rwth-aachen.de/must/files/MUST-v${MUST_VERSION}.tar.gz && \
    tar -xf MUST-v${MUST_VERSION}.tar.gz && mkdir -p MUST-v${MUST_VERSION}/build

RUN cd MUST-v${MUST_VERSION} && \
    cd build && \
    CC=clang CXX=clang++ MPICH_CC=clang MPICH_CXX=clang++ OMPI_CC=clang OMPI_CXX=clang \
      cmake .. -DCMAKE_INSTALL_PREFIX=/opt/must -DUSE_BACKWARD=ON -DENABLE_TESTS=ON -DENABLE_FORTRAN=ON -DLLVM_FILECHECK_PATH=$(which FileCheck) -DCMAKE_BUILD_TYPE=Release && \
    make install -j8 install-prebuilds

# Run checks as non-privileged user (required for OpenMPI)
RUN useradd -ms /bin/bash user
RUN chown -R user /build
USER user

RUN cd MUST-v${MUST_VERSION}/build && \
    OMPI_MCA_rmaps_base_oversubscribe=true \
    make check-minimal -j8

USER root
RUN rm -r /build

# Run container as non-privileged user
USER user
WORKDIR /home/user
