# MUST Docker Images

Provides different MUST docker images.

## Registry

The registry with all images is available here:
<https://git-ce.rwth-aachen.de/hpc-public/must-containers/container_registry>

## CI: Select Images to Build

Check [.gitlab-ci.yml](.gitlab-ci.yml) and update `LLVM_VERSION`, `MUST_VERSION`, `MPI_FLAVOR` accordingly.
Currently, only LLVM as compiler is supported.

## Build Image Manually

```
docker build -f Dockerfile -t my_image_tag \
       --build-arg MPI_FLAVOR=${MPI_FLAVOR} \
       --build-arg LLVM_VERSION=${LLVM_VERSION} \ 
       --build-arg MUST_VERSION=${MUST_VERSION} .
```
